# HTML-Essentials-with-styles

## Task list

- [X] Install any IDE or use the one that is already installed (for example, Visual Studio Code)
- [X] Create an "index.html" file and an "assets" folder for the images you will use in the project
  - [X] Add header section
  - [X] Add Hero block
  - [X] Add main section
  - [X] Add learn-more section
  - [X] Add footer